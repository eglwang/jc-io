#ifndef TASKPOOL_H
#define TASKPOOL_H
#include <Ticker.h>
#include "Lily/Lily_help.h"


// #define stop_schedule ticker.detach()
// #define resume_schedule ticker.attach_ms(1, lily_tick)

extern Ticker ticker;
extern bool in_ticking;

/*
#define Tasks_LEN 32

typedef int(*Tasks_def)();//return 0 meas task ended
typedef int(*TasksArg_def)(void*);//add a Task with a parameter
typedef int (*Arg_Tasks_def)(int argc, char* argv[]);
extern Tasks_def tasks_[Tasks_LEN];
extern int front, rear;
void addTask_(Tasks_def task);
void endTask_(Tasks_def task);
char hadTask_(Tasks_def task);
void endTaskAt_(char index);

void addTaskArg(TasksArg_def f, void* arg);
void remove_a_timer(Tasks_def task);
void lily_tick();
int add_timer_once(Tasks_def task, int count);

int change_quick_timer_count(Tasks_def timer, int newCount);

int create_or_change_quick_timer_count(Tasks_def timer, int newCount);


#define Hz(x) (1000/x)
#define Second(s) (1000*s)
void public_a_timer(Tasks_def task, unsigned int period,int rand=0);
void remove_a_timer(Tasks_def task);
*/

#define remove_a_timer remove_timer
int had_timer(Tasks_def task);
void change_timer_per(Tasks_def task,unsigned int newPer);

bool stop_schedule();
bool resume_schedule();

#endif
