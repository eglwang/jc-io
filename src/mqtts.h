#ifndef MQTTS_H
#define MQTTS_H
#include <PubSubClient.h>
#define dev "dev3"

extern PubSubClient mqtt_client;
extern String client_id;
extern unsigned long sync_start_time, sync_end_time;
extern time_t sys_time_base;

int mqtt_loop();
int cmd_set(int n, char *args[]);
void mqtt_setup(void (*callback)(char *topic, byte *payload, unsigned int length));
bool test_config();
void mqtt_out(const char *msg);
void mqtt_out(String &msg);
time_t service_time();
int start_sync_time();


#endif
