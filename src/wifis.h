#ifndef WIFIS_H
#define WIFIS_H
#include <ESP8266WiFi.h>

// void wifi_disconnected(const WiFiEventStationModeDisconnected &e);
// void wifi_connected(const WiFiEventStationModeDisconnected &e);
int setup_wifi(int try_time = 2);
void wifi_scan(bool connect = true);
// int wifi_fixed();
int wifi_connect(const char *ssid, const char *password, int timeout = 15);
int cmd_wifi(int n, char *args[]);
extern String ssid;
extern String password;
extern int wlans_len;

#endif