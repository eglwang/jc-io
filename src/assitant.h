#ifndef ASSITANT_H
#define ASSITANT_H
#include <Arduino.h>
#include <time.h>
#include "taskpool.h"

#define outs souts
#define out_ sout_

#define xouts Serial.println
#define xout_ Serial.print

typedef struct tm* datetime;
typedef struct tm datetime_t;

#define Arg(i) String(args[i])
// #define Lily_cmd_len 16
// typedef int (*CmdDef)(int n, char *args[]);

// int new_cmd(const char *name, CmdDef f);
int cmd_test(int n, char *args[]);
int cmd_restart(int n, char *arg[]);
// void cmd_in(char c);
// int do_cmd(char *raw);
// int cmd_help(int n, char *args[]);

#define new_cmd public_a_cmd_link
#define do_cmd lily_do
#define cmd_in lily_in

int serial_query();
bool is_int(const char *s);
// int str_split(char *str, char split_char);

void sout_(const char*s);
void sout_(const String &s);
void sout_(const char s);
void sout_(const int s);
void sout_(const float s);

void souts(const char*s);
void souts(const String &s);
void souts(const char s);
void souts(const int s);
void souts(const float s);


void str_tm(char *tx, datetime t);
void str_tm(char *tx, time_t t);
int tm_from_str(datetime t, char *str);

#endif
