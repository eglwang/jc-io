#ifndef MAIN_H
#define MAIN_H

#include <Arduino.h>
#include "Lily/Lily_help.h"
#define SD2 9
#define SD3 10
extern const int __firmware_version;

#define var auto

#define debug_var(x)   \
    {                  \
        xout_(#x "="); \
        xouts(x);      \
    }

extern const char *config_file;
extern const char *config_file_back;
extern const char *sensor_data_local;
extern const char *sensor_data_info;
// extern void (*_rep_data_hook)(String &data_str);
// extern int sensor_n;
int event_wifi_connected();
int shine_blink();
int rep();
// int read_sensor(char *tx);
void on_message(char *topic, byte *payload, unsigned int length);
int cmd_led(int n, char *args[]);
int cmd_rep(int n, char *args[]);
int cmd_test_ad(int n, char *args[]);
void config_setup();
int register_this_dev();
String parse_string(String &s, String &key);
String parse_string(String &s, const char *key);
int cmd_sleep(int n, char *args[]);
int load_config();
int file_append(const char *s, String end = "\n");
int file_prepend(const char *s, String end = "\n");

int timer_sleep();
String cmd_search_file(const char *item);
typedef bool (*_File_search_callback_handle)(String &file_row);
String cmd_search_file(_File_search_callback_handle callback_handle, int &out_line);

int clean_local_data(int count, time_t from_time);
int update_local_info(time_t time);
int sensor_data_dump(const char *data);
int cmd_beep(int n, char *args[]);

int cast_to_pin(const String &);

#endif
