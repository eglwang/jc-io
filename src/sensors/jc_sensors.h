#ifndef JCSENSOR_H
#define JCSENSOR_H

#include <Arduino.h>

#ifndef max_sensor_length
#define max_sensor_length 4
#endif
int cast_to_pin(const String &s);

class JCSensor
{
public:
    byte id;
    virtual int setup(String &config){return 0;};
    virtual String read(){return "";};
    virtual ~JCSensor(){};
};
#endif
