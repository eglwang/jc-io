#ifndef JCS_BUILTIN_H
#define JCS_BUILTIN_H

#include "DHTesp.h"
#include "jc_sensors.h"

class JCSensor_DHT11 : public JCSensor
{
public:
    virtual int setup(String &param);
    virtual String read();
    virtual ~JCSensor_DHT11();

private:
    DHTesp *dht = NULL;
};

class JCSensor_ADC : public JCSensor
{
public:
    virtual int setup(String &param);
    virtual String read();
    virtual ~JCSensor_ADC(){};

protected:
    String tag;
};
class JCSensor_NADC : public JCSensor_ADC
{
public:
    virtual String read();
};

#endif