#ifndef JCS_BMP_H
#define JCS_BMP_H
#include <Adafruit_BMP280.h>
#include "jc_sensors.h"

class JCSensor_BMP280 : public JCSensor
{
public:
    static Adafruit_BMP280 *__bmp;
    static int ins_count;
    virtual ~JCSensor_BMP280();
    virtual int setup(String &config);
    virtual String read();
    static int bmp_detail();

protected:
    Adafruit_BMP280 *bmp = NULL;
};

#endif
