

#ifndef SENSOR_MODULES_H
#define SENSOR_MODULES_H

#include "jcs_builtin.h"
#include "jcs_bmp.h"
#include "jcs_vl53l0x.h"

/*
to add a new sensor module:
    1. create a new class named "JCSensor_<your sensor type>", which inherited from class JCSensor.
    2. rewrite your setup(), and read() functions,
         the prior one used to setup your sensor, and the later one to read data.
    3. add a #include"<your class header>" statement above here
    4. append a loads(<your class tag>, <your class type>) tuple to modules following.
*/

#define modules loads(adc, ADC), loads(nadc, NADC), loads(dht11, DHT11), \  
                loads(bmp, BMP280), \
                loads(vlx, Vl53l0x)

#endif