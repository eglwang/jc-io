#ifndef JCS_VL53L0X_H
#define JCS_VL53L0X_H
#include "Adafruit_VL53L0X.h"
#include "jc_sensors.h"

// #define __vl53l0x_enable_alert 0
#ifndef __vl53l0x_enable_alert
#define __vl53l0x_enable_alert 1
#endif

#if __vl53l0x_enable_alert
#include "Lily/Lily_help.h"
#endif

typedef enum
{
    WaitForTrigger = 0,
    DuringTrigger,
    OnAction,
    WaitLeft
} _VLX_State;

class JCSensor_Vl53l0x : public JCSensor
{
public:
    static Adafruit_VL53L0X *__lox;
    static int ins_count;
    virtual ~JCSensor_Vl53l0x();
    virtual int setup(String &config);
    virtual String read();
    static int _fun_tof();

protected:
    static int read_data(Adafruit_VL53L0X *_lox);
    Adafruit_VL53L0X *lox = NULL;
#if __vl53l0x_enable_alert
    // static for alert
    static bool _vlx_monitor_on;
    static _VLX_State _vlx_state;
    static long int _vlx_last_enter_time;
    static Pipe _vlx_monitor_trigger_pipe;
    static int _vlx_task_monitor();
    static int _monitor_setup();
    static int _fun_lox_monitor(int mode);
    // int __read_data();
    static int _lox_warning_threshold;
    static int _lox_warning_threshold2;
    static int _lox_holding_time;
    static int _lox_letting_time;

#endif
};

#endif
