#ifndef HASHLIB_H
#define HASHLIB_H

#include<stdint.h>
typedef uint32_t HASH;

extern HASH(*getHashCodeS)(const char* byteStream);

extern HASH(*getHashCode)(const uint8_t* byteStream, unsigned int len);

HASH RSHash(const uint8_t* str, unsigned int len);

/* End Of RS Hash Function */

HASH JSHash(const uint8_t* str, unsigned int len);

/* End Of JS Hash Function */

HASH PJWHash(const uint8_t* str, unsigned int len);

/* End Of  P. J. Weinberger Hash Function */

HASH ELFHash(const uint8_t* str, unsigned int len);

/* End Of ELF Hash Function */

HASH BKDRHash(const uint8_t* str, unsigned int len);

/* End Of BKDR Hash Function */

HASH SDBMHash(const uint8_t* str, unsigned int len);

/* End Of SDBM Hash Function */

HASH DJBHash(const uint8_t* str, unsigned int len);

/* End Of DJB Hash Function */

HASH DEKHash(const uint8_t* str, unsigned int len);

/* End Of BP Hash Function */

HASH FNVHash(const uint8_t* str, unsigned int len);

/* End Of FNV Hash Function */

HASH APHash(const uint8_t* str, unsigned int len);

/* End Of AP Hash Function */

HASH SDBMHashS(const uint8_t* str);

// RS Hash Function
HASH RSHashS(const uint8_t* str);

// JS Hash Function
HASH JSHashS(const uint8_t* str);

// P. J. Weinberger Hash Function
HASH PJWHashS(const uint8_t* str);

// ELF Hash Function
HASH ELFHashS(const uint8_t* str);

// BKDR Hash Function
HASH BKDRHashS(const char* str);

// AP Hash Function
HASH APHashS(const uint8_t* str);

#endif
