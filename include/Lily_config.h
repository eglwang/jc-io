#ifndef LILY_CONFIG_H
#define LILY_CONFIG_H
#define Tasks_LEN 32
#define _VersionMsg "Chickdee"
#define tick_freq 1000
#define in_debug
#define TX_len 256
#define tx_len TX_len
#define max_cmd_len 127
#define NOALIGN 1
#endif