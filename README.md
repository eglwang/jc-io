
# jc-io

<hr>

<span style="background-image: linear-gradient(to right, #ffc107, #0dcaf0);-webkit-background-clip: text;color: transparent;font-size: 30px;font-weight:bold;">
Canary & JC-IO
</span>

A smart node in a IOT network, based on MQTT, working on ESP8266.  
The node is designed to collect sensor data and upload to cloudy, also it has the ability to excute commands dispatched from cloudy or other nodes (thanks to the contribution of [Lily](https://gitee.com/eglwang/Lily)), which makes the node can interact with other nodes, so it looks so intellgent.  

The branch "jc-st" implements same functions via paho-mqtt, but on STM32, the testing board is STM32G491.  

## Note

This project, named "jc-io", is created as a partition of project "Canary", which is aiming to build a IOT pivot based MQTT protocol.
The Canary is almost finished and you can visit the homepage of [Canary](https://glwang.site) for detail.

Here we give a screenshot from Canary
![chart](./doc/canary_homepage.jpeg)

## Setup

cause this project relies a submodule named [Lily](https://gitee.com/eglwang/Lily), run the following commands before you start your working.

```bash
git submodule init
git submodule update
```

## About this project

This project "jc-io" is only working on ESP8266, the branch "jc-st" however works on any ST32 MCU.

jc-io is build on platformIO, but you can easily rebuild it in Arduino-IDE, just need a few works.  
Some sensor like **DHT11** and **BMP280** are built in supported in jc-io, you can use them directory without writeing any code, but for others you may need to write the drives in youself, you can find the interface in [sensor.cpp](./src/sensor.cpp).  
> By default, the node assumes a **DHT11** was connected at pin **D2**(you can set the pin by send a command like `sensor dht11:d5` to change the pin to D5).  

The program has a mechanism call "glitch policy", it works if a sensor works abnormally, and the policy tells the program what to do if a glitch occurred, like power off, do nothing, or report a error.

> If the node fails to read the data from DHT11 for 5 times continuesly, it will send a glitch warning and power itself off automatically by default, you can set the policy to stop that however.

Right now, JC-IO has supported for Mi Home with the help of [bemfa cloud](https://cloud.bemfa.com/), that make you can retrieve sensor data by just asking XiaoAI (Xiaomi's voice assistant), or other platform's assistant.


## Support Drive (so far)

+ DHT11
+ BMP280
+ VL53L0X
+ IR Receiver

